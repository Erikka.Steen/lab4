package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private CellState[][] grid;
    private int cols;
    

    public CellGrid(int rows, int columns, CellState initialState) {
        this.cols = columns;
        this.rows = rows;
        this.grid = new CellState[rows][columns];
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                this.grid[row][col] = initialState;
                 
            }
            
        }

		// TODO Auto-generated constructor stub
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid kopi = new CellGrid(this.rows, this.cols, null);
        for (int row = 0; row < this.rows;row++) {
            for (int col = 0; col < this.cols; col++) {
                kopi.set(row, col, this.get(row,col));
                
            }
            
        }
        return kopi;
    }
    
}
